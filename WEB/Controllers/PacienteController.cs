﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ACCESOADATOS;
using RECURSOS;
using RECURSOS.Mensajes;

namespace WEB.Controllers
{
    public class PacienteController : Controller
    {
        ADPaciente pa = new ADPaciente();
        LogDeErrores log = new LogDeErrores();
        // GET: Paciente
        public ActionResult NuevoContacto()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NuevoContacto(string Cedula, string Nombres, string Apellidos, string Telefono)
        {
            try
            {
                pa.NuevoContacto(Cedula, Nombres, Apellidos, Telefono);
                ViewBag.mensaje = MensajesRecursos.NuevoRegistro;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.mensaje = MensajesRecursos.ErrorNuevoRegistro;
                log.LogMensaje("",(ViewBag.mensaje + ". " + ex.ToString()));
                return View();
            }
        }
    }
}