﻿using System;
using System.Configuration;
using System.IO;

namespace RECURSOS
{
    public class LogDeErrores
    {
        public void LogMensaje(string modulo, string mensaje)
        {
            //string path = System.ConfigurationManager.AppSettings["LogErrores"];
            string path = ConfigurationManager.AppSettings["LogErrores"];
            using (StreamWriter sw = File.AppendText(path + "LOG_" + modulo + "_" + System.DateTime.Now.ToString("dd-MM-yyyy")))
            {
                sw.WriteLine("");
                sw.WriteLine("Se ha generado el siguiente evento: " + mensaje);
                sw.WriteLine("");
                sw.WriteLine("registrado el : " + System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
                sw.WriteLine("");
                sw.WriteLine("=================================================================================================");
            }
        }
    }
}
