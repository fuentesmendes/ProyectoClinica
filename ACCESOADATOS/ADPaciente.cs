﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODELO;
using RECURSOS;
using RECURSOS.Mensajes;

namespace ACCESOADATOS
{
    public class ADPaciente
    {
        CLINICAEntities ce = new CLINICAEntities();
        LogDeErrores log = new LogDeErrores();


        public Boolean NuevoContacto(string Cedula, string Nombres, string Apellidos, string Telefono)
        {
            try
            {
                ce.SP_NUEVO_CONTACTO(Cedula, Nombres, Apellidos, Telefono);
                var mensaje = MensajesRecursos.NuevoRegistro;
                log.LogMensaje("Nuevo Contacto",mensaje);
                return true;
            }
            catch (Exception ex)
            {
                var mensaje = MensajesRecursos.ErrorNuevoRegistro;
                log.LogMensaje("Nuevo Contacto", (ex.ToString() + " " + mensaje));
                return false;
            }
        }

    }
}
